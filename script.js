alert('Primeiro tutorial VueJS da disciplina de DAW1')

new Vue({
    el: '#app',
    template: `
    <div class="container">
        <h1>Comentários</h1>
        <div class="form-todo form-group">
            <p>
                <input type="text" placeholder="Nome" name="Autor" class="form-control" v-model="name">
            </p>

            <p>
                <input type="text" name="comentario" placeholder="Comentário" class="form-control" v-model="message"/>
            </p>

            <button v-on:click="addComment" type="submit" class="btn btn-primary">Enviar</button>

        </div>
        <br>
        <div class="list-group">
            <div class="list-group-item" v-for="(comment, index) in comments">

                <span class="comment__auhor"> Autor: <strong> {{ comment.name }} </strong></span>

                <p> {{ comment.message }} </p>

                <div>
                    <a href="#" title="Excluir" v-on:click="removeComment(index)">Excluir</a>
                </div>

            </div>
        </div>
    </div>
    `,
    data() {
        return {
            comments: [
                {
                    name: 'Allanda',
                    message: 'Olá'
                }
            ],
            name: '',
            message: ''
        }
    },
    methods: {
            addComment(){
                this.comments.push({
                    name: this.name,
                    message: this.message
                });
            },
            removeComment(index){
                this.comments.splice(index, 1)
            }
    }
})